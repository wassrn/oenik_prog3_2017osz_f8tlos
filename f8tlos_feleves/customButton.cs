﻿// <copyright file="CustomButton.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace f8tlos_feleves
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;

    public sealed class CustomButton : Grid
    {
        private BitmapImage texNormal;
        private BitmapImage texHover;
        private BitmapImage texHoverOnly;
        private BitmapImage texOnClick;

        private Image hoverImage;
        private double hoverAlpha = 0f;
        private float pulseSpeed = 2f;
        private Label labelText;

        private DispatcherTimer dt;

        private bool isMouseHovering = false;
        private bool isMouseBeignPressed = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomButton"/> class.
        /// </summary>
        public CustomButton()
        {
            this.dt = new DispatcherTimer();
            this.dt.Interval = new TimeSpan(0, 0, 0, 0, 30);
            this.dt.Tick += this.Update;
            this.dt.Start();
            this.texNormal = new BitmapImage(new Uri(@"pack://application:,,,/Resources/customButton_normal.png"));
            this.texHover = new BitmapImage(new Uri(@"pack://application:,,,/Resources/customButton_hover.png"));
            this.texHoverOnly = new BitmapImage(new Uri(@"pack://application:,,,/Resources/customButton_hover_only.png"));
            this.texOnClick = new BitmapImage(new Uri(@"pack://application:,,,/Resources/customButton_clicked.png"));
            this.Background = new ImageBrush(this.texNormal);
            this.Width = 178;
            this.Height = 120;
            this.hoverImage = new Image()
            {
                Source = this.texHoverOnly,
                Opacity = 0f
            };
            base.Children.Add(this.hoverImage);

            this.labelText = new Label()
            {
                FontSize = 20,
                Foreground = Brushes.White,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                VerticalContentAlignment = VerticalAlignment.Center,
                Padding = new Thickness(10, 10, 10, 16)
            };
            base.Children.Add(this.labelText);
            this.MouseDown += this.OnMouseDown;
            this.MouseUp += this.OnMouseUp;
            this.MouseEnter += this.OnMouseEnter;
            this.MouseLeave += this.OnMouseLeave;
        }

        /// <summary>
        /// Gets children
        /// </summary>
        [Obsolete("Use Text property instead.", true)]
        public new UIElementCollection Children
        {
            get; private set;
        }

        /// <summary>
        /// Gets or sets gomb szövege
        /// </summary>
        public string Text
        {
            get
            {
                return (string)this.labelText.Content;
            }

            set
            {
                this.labelText.Content = value;
            }
        }

        /// <summary>
        /// Gets or sets villogás sebessége
        /// </summary>
        public float HoverPulseSpeed
        {
            get
            {
                return this.pulseSpeed;
            }

            set
            {
                this.pulseSpeed = Math.Abs(value);
            }
        }

        private double HoverAlpha
        {
            set
            {
                if (this.hoverAlpha != value)
                {
                    this.hoverAlpha = value;
                    this.hoverImage.Opacity = this.hoverAlpha;
                }
            }
        }

        private bool IsMouseBeignPressed
        {
            set
            {
                if (this.isMouseBeignPressed != value)
                {
                    this.isMouseBeignPressed = value;
                    if (value)
                    {
                        this.Background = new ImageBrush(this.texOnClick);
                    }
                    else
                    {
                        this.Background = new ImageBrush(this.texNormal);
                    }
                }
            }
        }

        /// <summary>
        /// Click esemény kezelője.
        /// </summary>
        public event MouseButtonEventHandler Click;

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            this.IsMouseBeignPressed = true;

            if (this.Click != null)
            {
                this.Click(sender, e);
            }
        }

        private void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            this.IsMouseBeignPressed = false;
        }

        private void OnMouseEnter(object sender, MouseEventArgs e)
        {
            this.isMouseHovering = true;
        }

        private void OnMouseLeave(object sender, MouseEventArgs e)
        {
            this.isMouseHovering = false;
        }

        private void Update(object sender, EventArgs e)
        {
            if (this.isMouseHovering && !this.isMouseBeignPressed)
            {
                this.HoverAlpha = (Math.Abs(Math.Sin(TimeSpan.FromTicks(DateTime.Now.Ticks).TotalSeconds * this.pulseSpeed)) / 2) + 0.5f;
            }
            else
            {
                this.HoverAlpha = 0f;
            }
        }
    }
}
