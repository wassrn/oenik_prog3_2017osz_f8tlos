﻿// <copyright file="GameWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace f8tlos_feleves
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for Game.xaml
    /// Létrejön a játékablak amit a GameVisual.cs menedzsel
    /// </summary>
    public partial class GameWindow : Window
    {
        public GameWindow()
        {
            this.InitializeComponent();
        }
    }
}
