﻿// <copyright file="AddUserName.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace f8tlos_feleves
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Játékos és pontszáma hozzáadása egy .txt filehoz
    /// </summary>
    public partial class AddUserName : Window
    {
        private int score;

        /// <summary>
        /// Initializes a new instance of the <see cref="AddUserName"/> class.
        /// </summary>
        /// <param name="score">játékos által elért pontszám</param>
        public AddUserName(int score)
        {
            this.InitializeComponent();

            this.score = score;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!File.Exists("highscores.txt"))
            {
                FileStream x = File.Create("highscores.txt");
                x.Close();
            }

            string betoltes = File.ReadAllText("highscores.txt");
            betoltes += (string)this.user.Text + " " + this.score + Environment.NewLine;
            File.WriteAllText("highscores.txt", betoltes);

            this.Close();
        }
    }
}
