﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace f8tlos_feleves
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// Főmenü new game, high scores, exit gombokkal
    /// </summary>
    public partial class MainWindow : Window
    {
        private CustomButton cb0;
        private CustomButton cb1;
        private CustomButton cb2;

        public MainWindow()
        {
            this.InitializeComponent();

            StackPanel sp = new StackPanel()
            {
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Margin = new Thickness(0, 0, 0, 0)
            };

            this.cb0 = new CustomButton() { Text = "NEW GAME" };
            this.cb1 = new CustomButton() { Text = "HIGH SCORE" };
            this.cb2 = new CustomButton() { Text = "EXIT" };

            this.cb0.Click += this.OnNewGameClick;
            this.cb1.Click += this.OnHighScoresClick;
            this.cb2.Click += this.OnExitClick;

            sp.Children.Add(this.cb0);
            sp.Children.Add(this.cb1);
            sp.Children.Add(this.cb2);

            this.grid.Children.Add(sp);
        }

        private void OnHighScoresClick(object sender, MouseButtonEventArgs e)
        {
            HighScores highscores = new HighScores();
            highscores.Show();
            this.Close();
        }

        private void OnExitClick(object sender, RoutedEventArgs e)
        {
            // Application.Current.Shutdown();
            this.Close();
        }

        private void OnNewGameClick(object sender, RoutedEventArgs e)
        {
            GameWindow jatekablak = new GameWindow();
            jatekablak.Show();
            this.Close();
        }
    }
}
