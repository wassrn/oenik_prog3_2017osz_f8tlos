﻿// <copyright file="Drink.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace f8tlos_feleves
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    public sealed partial class GameVisual
    {
        private class Drink : GameObject
        {
            private const float SPEED = 10;
            private const float ACCELERATION = 0.5f;
            private const double STARTY = 30;
            private const double SIZEX = 89;
            private const double SIZEY = 289;

            private static ImageSource sprite = null;
            private double desPosY;
            private int score;
            private Random ran = new Random();
            private bool whichImage;
            private int addSec;

            public Drink(GameVisual game, bool which)
            {
                this.Game = game;
                this.WhichImage = which;
                Point start = new Point(RND.Next(0 + ((int)SIZEX / 2), (int)this.Game.ActualWidth - ((int)SIZEX / 2)), STARTY);
                this.position = start;
                this.desPosY = start.Y;
                sprite = null;
                if (this.whichImage)
                {
                    this.score = 300;
                    this.addSec = 2;
                }
                else
                {
                    this.score = 250;
                    this.addSec = 1;
                }

                this.geometry = new RectangleGeometry(new Rect(new Point(-SIZEX / 6, -SIZEY / 6), new Size(SIZEX / 3, SIZEY / 3)));
            }

            public override Geometry GetGeometry
            {
                get
                {
                    this.geometry.Transform = new TranslateTransform(this.position.X, this.position.Y);
                    return this.geometry;
                }
            }

            public ImageSource GetSprite
            {
                get
                {
                    if (Drink.sprite == null)
                    {
                        if (this.whichImage)
                        {
                            sprite = new BitmapImage(new Uri(@"pack://application:,,,/Resources/wineBottle.png"));
                        }
                        else
                        {
                            sprite = new BitmapImage(new Uri(@"pack://application:,,,/Resources/beer02.png"));
                        }
                    }

                    return sprite;
                }
            }

            public int Score
            {
                get
                {
                    return this.score;
                }
            }

            public bool WhichImage
            {
                get
                {
                    return this.whichImage;
                }

                set
                {
                    this.whichImage = value;
                }
            }

            public int AddSec
            {
                get
                {
                    return this.addSec;
                }
            }

            public void Move()
            {
                this.desPosY += SPEED;
                this.position.Y = this.Lerp(this.position.Y, this.desPosY, ACCELERATION);
            }

            public bool OutOfWindow()
            {
                if (this.position.Y >= this.Game.ActualHeight)
                {
                    return true;
                }

                return false;
            }
        }
    }
}
