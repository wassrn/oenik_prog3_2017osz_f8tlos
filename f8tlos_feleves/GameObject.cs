﻿// <copyright file="GameObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace f8tlos_feleves
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    public sealed partial class GameVisual
    {/// <summary>
    /// Órán leírt módon működik az osztály.
    /// </summary>
        private abstract class GameObject
        {
            /// <summary>
            /// Játékos pozíciója.
            /// </summary>
            protected Point position;
            protected Geometry geometry;

            public virtual Geometry GetGeometry { get; set; }

            protected GameVisual Game { get; set; }

            public delegate void GameObjectEventHandler(GameObject obj);

            /// <summary>
            /// Visszaadja hogy azt objektum ütközött-e egy megadott másikkal.
            /// </summary>
            /// <param name="other">A másik objektum</param>
            /// <returns>Történt-e ütközés</returns>
            public bool Collide(GameObject other)
            {
                PathGeometry intersection = Geometry.Combine(
                    this.GetGeometry.GetFlattenedPathGeometry(),
                    other.GetGeometry.GetFlattenedPathGeometry(),
                    GeometryCombineMode.Intersect,
                    null);

                return intersection.GetArea() > 0;
            }

            protected double Lerp(double a, double b, double t)
            {
                return a + ((b - a) * t);
            }
        }
    }
}
