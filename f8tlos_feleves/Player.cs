﻿namespace f8tlos_feleves
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    public sealed partial class GameVisual
    {
        private class Player : GameObject
        {
            private const float SPEED = 20;
            private const float ACCELERATION = 0.5f;
            private const double SIZEX = 80;
            private const double SIZEY = 120;

            private static ImageSource sprite = null;
            private double desPosX;
            private double gyorsito;

            public Player(GameVisual game)
            {
                this.Game = game;
                Point start = new Point((int)this.Game.ActualWidth / 2, (int)this.Game.ActualHeight - 150);
                this.position = start;
                this.gyorsito = 0;
                this.desPosX = start.X;

                this.geometry = new RectangleGeometry(new Rect(new Point(-SIZEX / 2, -SIZEY / 2), new Size(SIZEX, SIZEY)));
            }

            public override Geometry GetGeometry
            {
                get
                {
                    this.geometry.Transform = new TranslateTransform(this.position.X, this.position.Y);
                    return this.geometry;
                }
            }

            public ImageSource GetSprite
            {
                get
                {
                    if (Player.sprite == null)
                    {
                        sprite = new BitmapImage(new Uri(@"pack://application:,,,/Resources/playersprite.png"));
                    }

                    return sprite;
                }
            }

            public void Move(bool left, bool right)
            {
                if (left)
                {
                    this.desPosX -= SPEED;
                }

                if (right)
                {
                    this.desPosX += SPEED;
                }

                this.gyorsito = this.Lerp(this.position.X, this.desPosX, ACCELERATION);

                if (this.gyorsito > SIZEX / 2 && this.gyorsito < this.Game.ActualWidth - (SIZEX / 2))
                {
                    this.position.X = this.gyorsito;
                }
            }
        }
    }
}
