﻿// <copyright file="HighScores.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace f8tlos_feleves
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for HighScores.xaml
    /// Létrehozza a Top5 listát a highscores.txt-ből
    /// </summary>
    public partial class HighScores : Window
    {
        public HighScores()
        {
            this.InitializeComponent();

            StackPanel sp = new StackPanel()
            {
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Margin = new Thickness(0, 0, 0, 0)
            };

                Label output = new Label()
                {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                    Foreground = Brushes.Gray,
                    FontSize = 25
            };

            Label output1 = new Label()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.Gray,
                FontSize = 25
            };
            Label output2 = new Label()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.Gray,
                FontSize = 25
            };
            Label output3 = new Label()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.Gray,
                FontSize = 25
            };
            Label output4 = new Label()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.Gray,
                FontSize = 25
            };

            string[] results = this.TxtLoader();
            if (results != null)
            {
                if (results.Length > 0)
                {
                    output.Content = "1. " + results[0];
                }

                if (results.Length > 1)
                {
                    output1.Content = "2. " + results[1];
                }

                if (results.Length > 2)
                {
                    output2.Content = "3. " + results[2];
                }

                if (results.Length > 3)
                {
                    output3.Content = "4. " + results[3];
                }

                if (results.Length > 4)
                {
                    output4.Content = "5. " + results[4];
                }
            }

            sp.Children.Add(output);
            sp.Children.Add(output1);
            sp.Children.Add(output2);
            sp.Children.Add(output3);
            sp.Children.Add(output4);

            /*   for (int i = 0; i < results.Length; i++)
               {
                   if (i<10)
                   {
                       output.Content = i + ". " + results[i];
                       sp.Children.Add(output);
                   }
               }*/

            CustomButton back = new CustomButton { Text = "BACK", VerticalAlignment = VerticalAlignment.Bottom };
            back.Click += this.OnBackClick;

            this.grid.Children.Add(sp);
            this.grid.Children.Add(back);
        }

        private void OnBackClick(object sender, MouseButtonEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }

        private string[] TxtLoader()
        {
            if (File.Exists("highscores.txt"))
            {
                string[] results = File.ReadAllLines("highscores.txt");
                int pont;
                string atm;

                int[] numbers = new int[results.Length];

                for (int i = 0; i < results.Length; i++)
                {
                    numbers[i] = int.Parse(results[i].Split(' ')[1]);
                }

                for (int i = 0; i < results.Length - 1; i++)
                {
                    for (int j = i + 1; j < results.Length; j++)
                    {
                        if (numbers[i] < numbers[j])
                        {
                            atm = results[i];
                            pont = numbers[i];
                            results[i] = results[j];
                            numbers[i] = numbers[j];
                            results[j] = atm;
                            numbers[j] = pont;
                        }
                    }
                }

                return results;
            }
            else
            {
                return null;
            }
        }
    }
}
