﻿// <copyright file="GameVisual.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace f8tlos_feleves
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;

    public sealed partial class GameVisual : FrameworkElement
    {
        private static Random RND = new Random();
        private List<Drink> drinksCollected;
        private Player player;
        private DispatcherTimer dt;
        private DispatcherTimer countdown;
        private int timeLeft;
        private bool left = false;
        private bool right = false;
        private int score;
        private int elapsedTicks;
        private int minimalLimit;

        public GameVisual()
        {
            this.drinksCollected = new List<Drink>();
            this.Loaded += this.OnLoaded;
        }

        /// <summary>
        /// Gets a játékos pontszámát.
        /// </summary>
        public int Score
        {
            get
            {
                return this.score;
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).KeyDown += this.OnKeyDown;
            Window.GetWindow(this).KeyUp += this.OnKeyUp;

            this.player = new Player(this);

            this.score = 0;
            this.elapsedTicks = 0;
            this.minimalLimit = 85;
            this.timeLeft = 30;

            this.dt = new DispatcherTimer();
            this.dt.Interval = new TimeSpan(0, 0, 0, 0, 30);
            this.dt.Tick += this.Update;
            this.dt.Start();

            this.countdown = new DispatcherTimer();
            this.countdown.Interval = new TimeSpan(0, 0, 0, 1);
            this.countdown.Tick += this.Countdown_Tick;
            this.countdown.Start();
        }

        /// <summary>
        /// Megadja a játék leállási feltételét.
        /// </summary>
        /// <param name="sender"> - </param>
        /// <param name="e"> _ </param>
        private void Countdown_Tick(object sender, EventArgs e)
        {
            this.timeLeft--;
            if (this.timeLeft <= 0)
            {
                AddUserName addUser = new AddUserName(this.score);
                this.countdown.Stop();
                this.dt.Stop();
                addUser.ShowDialog();
                MainWindow back = new MainWindow();
                back.Show();
                Application.Current.Windows[0].Close();
            }
        }

        private void Update(object sender, EventArgs e)
        {
            this.elapsedTicks++;
            if (RND.Next(100) == 0 || this.elapsedTicks > this.minimalLimit)
            {
                this.drinksCollected.Add(new Drink(this, RND.Next(2) == 0));
                this.elapsedTicks = 0;
            }

            foreach (Drink dri in this.drinksCollected.ToArray())
            {
                dri.Move();
                if (this.player.Collide(dri))
                {
                    this.score += dri.Score;
                    this.timeLeft += dri.AddSec;
                    this.drinksCollected.Remove(dri);
                    if (this.minimalLimit > 35)
                    {
                        this.minimalLimit--;
                    }
                }

                if (dri.OutOfWindow())
                {
                    this.timeLeft -= 3;
                    this.drinksCollected.Remove(dri);
                }
            }

            this.player.Move(this.left, this.right);

            this.InvalidateVisual();
        }

        private void OnKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.A || e.Key == Key.Left)
            {
                this.left = false;
            }

            if (e.Key == Key.D || e.Key == Key.Right)
            {
                this.right = false;
            }
        }

        private void OnKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.A || e.Key == Key.Left)
            {
                this.left = true;
            }

            if (e.Key == Key.D || e.Key == Key.Right)
            {
                this.right = true;
            }
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            foreach (Drink drink in this.drinksCollected)
            {
                drawingContext.DrawImage(drink.GetSprite, drink.GetGeometry.Bounds);
            }

            if (this.player != null)
            {
                drawingContext.DrawImage(this.player.GetSprite, this.player.GetGeometry.Bounds);
            }

            FormattedText scores = new FormattedText("Score: " + this.Score.ToString(), CultureInfo.CurrentCulture, FlowDirection.RightToLeft, new Typeface("Verdana"), 55, Brushes.White);
            drawingContext.DrawText(scores, new Point(this.ActualWidth, 0));

            FormattedText time = new FormattedText("00: " + this.timeLeft.ToString(), CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Verdana"), 55, Brushes.White);

            drawingContext.DrawText(time, new Point(10, 0));
        }
    }
}
